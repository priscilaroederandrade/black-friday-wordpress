<?php
//custom logo
function config_custom_logo() {
    add_image_size( 'custom-image-size' , 107 , 41 , true );
    $defaults = array(
        'height' => 'custom-image-size',
        'width' => 'custom-image-size',
        'flex-height' => true,
        'flex-width' => true,
        'header-text' => array(  ),
    );
    add_theme_support( 'custom-logo' , $defaults );
}
add_action( 'after_setup_theme' , 'config_custom_logo' );

function url_cdn_projeto() {
	return "https://www.tecmundo.com.br/blackfriday/";
  }


function _blackfriday_nzn_get_custom_logo() {
    if ( has_custom_logo() ) {
        $logo = wp_get_attachment_image_src( get_theme_mod( 'custom_logo' ) , 'full' );
		echo '<a class="custom-logo-link navbar-brand" href="https://tecmundo.com.br/blackfriday/" >';
        echo '<img class="custom-logo" src="' . esc_url( $logo[0] ) . '" width="' . $logo[1] . '" height="' . $logo[2] . '" alt="' . get_bloginfo( 'name' ) . '">';
        echo "</a>";
    } else {
        echo '<h1>';
        echo '<a href="' . get_site_url() . '">' . get_bloginfo( 'name' ) . '</a>';
        echo '</h1>';
    }
}
//menus
  function register_my_menus() {
	register_nav_menus(
	  array(
		'header-menu' => __( 'Header Menu' ),
		'extra-menu' => __( 'Extra Menu' )
	  )
	);
  }
  add_action( 'init', 'register_my_menus' );


    
add_theme_support( 'custom-header' );
$defaults = array(
	'default-image'          => '',
	'width'                  => 980,
	'height'                 => 60,
	'flex-height'            => true,
	'flex-width'             => true,
	'uploads'                => true,
	'random-default'         => false,
	'header-text'            => true,
	'default-text-color'     => true,
	'wp-head-callback'       => '',
	'admin-head-callback'    => '',
	'admin-preview-callback' => '',
);
add_theme_support( 'custom-header', $defaults );

add_theme_support( 'custom-footer' );
$defaults = array(
	'default-image'          => '',
	'width'                  => 980,
	'height'                 => 60,
	'flex-height'            => true,
	'flex-width'             => true,
	'uploads'                => true,
	'random-default'         => false,
	'header-text'            => true,
	'default-text-color'     => true,
	'wp-head-callback'       => '',
	'admin-head-callback'    => '',
	'admin-preview-callback' => '',
);
add_theme_support( 'custom-footer', $defaults );

add_theme_support( 'custom-background' );
$defaults = array(
    'default-color'          => '',
    'default-image'          => '',
    'default-repeat'         => '',
    'default-position-x'     => '',
    'default-attachment'     => '',
    'wp-head-callback'       => '_custom_background_cb',
    'admin-head-callback'    => '',
    'admin-preview-callback' => ''
);
add_theme_support( 'custom-background', $defaults );

add_theme_support( 'post-thumbnails' ); 

the_post_thumbnail();

the_post_thumbnail('thumbnail');       
the_post_thumbnail('medium');          
the_post_thumbnail('medium_large');    
the_post_thumbnail('large');           
the_post_thumbnail('full');            

the_post_thumbnail( array(100,100) );  //outras resoluoes

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Configurações de tema',
		'menu_title'	=> 'Configurações de tema',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Tema header configuração',
		'menu_title'	=> 'Header',
		'parent_slug'	=> 'theme-general-settings',
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Tema footer configuração',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'theme-general-settings',
	));
	
}

add_image_size( 'full-destaque', 825, 384, true ); // Unlimited Height Mode
add_image_size( 'destaque-principal1-3', 960, 566, true ); // Hard Crop Mode
add_image_size( 'destaque-principal2-3', 960, 566, true ); // Soft Crop Mode
add_image_size( 'destaques', 558, 242, true ); // Unlimited Height Mode
add_image_size( 'sidebar', 122, 94 , true); // Unlimited Height Mode



function _blackfriday_nzn_stylesheets()
{
	wp_enqueue_style( 'style', get_stylesheet_directory_uri() . '/style.css',  '1.0.0', 'all', false );
}
add_action('wp_enqueue_scripts', '_blackfriday_nzn_stylesheets');

function _blackfriday_nzn_assets() {
	global $wp_scripts;
	wp_enqueue_style( 'font', 'https://use.typekit.net/ylf4xqq.css','1.0.0', true );
	wp_enqueue_style( 'bootstrap', get_stylesheet_directory_uri() . '/assets/bootstrap/css/bootstrap.min.css','1.0.0', true );
	wp_enqueue_style( 'owl-carousel', get_stylesheet_directory_uri() . '/assets/css/owl.carousel.min.css','1.0.0', true );
	wp_enqueue_script( 'owl-carousel-js', get_template_directory_uri() . '/assets/js/owl.carousel.min.js', array('jquery'), '1.0.0', true);

	wp_enqueue_script( 'main-js', get_template_directory_uri() . '/assets/js/bundle.js', array(), '1.0.0', true);
	wp_enqueue_script( 'bootstrap-popper','https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js', array('jquery'), '1.0.0', true);
	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/assets/bootstrap/js/bootstrap.min.js', array('jquery'), '1.0.0', true);

}

add_action('wp_footer', '_blackfriday_nzn_assets');

//desativar guttenberg
function smartwp_remove_wp_block_library_css(){
    wp_dequeue_style( 'wp-block-library' );
    wp_dequeue_style( 'wp-block-library-theme' );
    wp_dequeue_style( 'wc-block-style' ); // Remove WooCommerce block CSS
} 
add_action( 'wp_enqueue_scripts', 'smartwp_remove_wp_block_library_css', 100 );


// set cannonical
// add_filter( 'wpseo_canonical', function( $canonical ) {

// 	if( '//tecmundo.com.br/blackfriday/' ==  get_option( 'home' ) ) {
// 	  return $canonical;
// 	}
  
// 	$canonical = preg_replace('#//[^/]*/#U', '//tecmundo.com.br/blackfriday/', trailingslashit( $canonical ) );
// 	return $canonical;
// });


function update_og_url($url) {
	return "https://www.tecmundo.com.br/blackfriday/";
  }
  add_filter('wpseo_opengraph_url', 'update_og_url', 10, 1);