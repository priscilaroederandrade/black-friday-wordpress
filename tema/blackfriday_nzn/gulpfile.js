// Require gulp packages
var gulp = require('gulp');
var sass = require('gulp-sass');
var js = require('gulp-uglify');
var imagemin = require('gulp-imagemin');
var browsersync = require('browser-sync');
var reload = browsersync.reload;
var concatCss = require('gulp-concat-css');
var gutil = require('gulp-util');
var cleanCSS = require('gulp-clean-css');


gulp.task('sass', function() {
    return gulp.src(['./assets/scss/style.scss'])
        .pipe(sass({ style: 'compressed' }).on('error', gutil.log))
        .pipe(concatCss('main.css'))
        .pipe(cleanCSS({ compatibility: 'ie8' }))
        .pipe(gulp.dest('./assets/css'))
});

gulp.task('js', function() {
    return gulp.src('./assets/js/bundle.js')
        .pipe(js())
        .pipe(gulp.dest('./assets/js/min'))
});

gulp.task('imagemin', function() {
    gulp.src('./assets/images/*')
        .pipe(imagemin())
        .pipe(gulp.dest('./assets/images'))
});

gulp.task('browser-sync', function() {
    browsersync.init({
        baseDir: 'gulp-wp.dev'
    });
});

gulp.task('reload', function() {
    browsersync.reload();
});

gulp.task('watch', function() {
    gulp.watch('./assets/sass/**/*.scss', ['sass']);
    gulp.watch('./assets/js/*.js', ['js']);
    gulp.watch(['./assets/sass/**/*.scss', './assets/js/*.js', '*.php'], ['reload']);
});

gulp.task('default', ['sass', 'js', 'imagemin', 'browser-sync', 'watch']);