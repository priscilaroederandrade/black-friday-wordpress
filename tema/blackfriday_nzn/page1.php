<?php get_header(); ?>
<section class="section-first ">
    <div class="percent per1"></div>
    <div class="percent per2"></div>
    <div class="percent per3"></div>

    <div class="container h-100-c">
        <div class="row  h-100 d-flex justify-content-center align-items-center">
            <div class="col-lg-6 d-flex align-items-lg-stretch align-items-center flex-column  bd-highlight">
                <?php the_post_thumbnail( 'full',  ['class' => 'img-fluid']); ?>
                <?php the_content(); ?>
            </div>
            <div class="col-lg-6 pt-lg-5 pb-lg-0  pl-lg-5 pr-lg-5 pt-3 pb-0 pr-0 pl-0">
                <h2 class="title-simple">Categorias mais acessadas:</h2>
                <?php get_template_part('partials/top_categories'); ?>
            </div>
            <div class="col-12 d-flex justify-content-center">
                <?php
                    $link = get_field('botao_primeira_dobra');
                    if($link){
                    $link_url = $link['url'];
                    $link_title = $link['title'];
                    $link_target = $link['target'] ? $link['target'] : '_self';
                ?>
                    <a class="btn-primary" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                        <?php echo esc_html( $link_title ); ?>
                    </a>
                    <?php 
                    }
                ?>
            </div>
            <a href="<?php echo get_field('id_ancora'); ?>" class="col-12 p-lg-5 pt-5 pb-3 d-flex justify-content-center">
                <div class="arrow">
                    <ul>
                        <li></li>
                        <li></li>
                    </ul>
                </div>
            </a>
        </div>
    </div>
    <div class="clientes-feed">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <?php get_template_part('partials/clientes'); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_template_part('partials/newsletter'); ?>
<section id="vejatodasasofertas">
    <div class="percent per4"></div>
    <div class="percent per5"></div>
    <div class="container">
        <div class="row">
            <div class="col-12  col-lg-5">
                <h2 class="title-big">
                    <?php echo get_field("titulo_asmelhores") ?>
                </h2>
                <p class="d-block">
                    <?php echo get_field("descricao_asmelhores") ?>
                </p>

                <?php
                $botaobotao_asmelhores = get_field("botao_asmelhores");
                if ($botaobotao_asmelhores) {
                    $link_url = $botaobotao_asmelhores['url'];
                    $link_title = $botaobotao_asmelhores['title'];
                    $link_target = $botaobotao_asmelhores['target'] ? $botaobotao_asmelhores['target'] : '_self';
                ?>
                    <a class="btn-primary" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                        <?php echo  $link_title; ?>
                    </a>
                    <?php 
                }
            ?>
            </div>
            <div class="col-12 p-0  offset-lg-1 col-lg-6">
                <?php get_template_part('partials/ofertas_selecionadas'); ?>
            </div>
        </div>
    </div>
</section>
<section id="livemelhoresofertas">
    <div class="percent per6"></div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3 class="title-mediun text-center">
                    <?php echo get_field("titulo_da_sessao") ?>
                </h3>
            </div>
        </div>
        <div class="row mt-lg-5 mt-3 d-flex align-items-end">
            <div class="col-12 col-lg-5">
                <h2 class="title-big">
                    <?php echo get_field("titulo") ?>
                </h2>
                <p class="d-block">
                    <?php echo get_field("descricao") ?>
                </p>
                <?php
            if (get_field("youtube_icone")) { ?>
                    <ul class="lives-plataformas">
                        <li>
                            <?php
                $youtube_link = get_field("youtube_link");
                if ($youtube_link) {
                    $link_url = $youtube_link['url'];
                    $link_title = $youtube_link['title'];
                    $link_target = $youtube_link['target'] ? $youtube_link['target'] : '_self';
                ?>
                                <a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                            <i class="icon-<?php echo get_field('youtube_icone') ?> icone"></i>
                            <span><?php echo $link_title; ?></span>
                        </a>
                                <?php 
                    }
                ?>
                        </li>
                        <li>
                            <?php
                        $igtv_link = get_field("igtv_link");
                        if ($igtv_link) {
                            $link_url = $igtv_link['url'];
                            $link_title = $igtv_link['title'];
                            $link_target = $igtv_link['target'] ? $igtv_link['target'] : '_self';
                        ?>
                                <a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                            <i class="icon-<?php echo get_field('igtv_icone') ?> icone"></i>
                            <span><?php echo $link_title; ?></span>
                        </a>
                                <?php 
                    }
                ?>
                        </li>
                    </ul>
                    <?php }
            ?>
                    <?php
                $botao = get_field("botao");
                if ($botao) {
                    $link_url = $botao['url'];
                    $link_title = $botao['title'];
                    $link_target = $botao['target'] ? $botao['target'] : '_self';
                ?>
                        <button type="button" class="btn-secondary" data-toggle="modal" data-target="#horarioLives">

                    <?php echo $link_title; ?>
                </button>
                        <?php 
                }
            ?>
            </div>
            <div class="col-12 col-lg-6 offset-lg-1">
                <?php the_field('video'); ?>

            </div>
        </div>
    </div>
</section>
<section id="cuponsdedesconto">
    <div class="percent per7"></div>
    <div class="percent per8"></div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="title-big text-center">
                    <?php echo get_field("titulo_cupons") ?>
                </h2>
                <p class="text-center">
                    <?php echo get_field("descricao_cupons") ?>
                </p>
                <div>
                    <?php get_template_part('partials/cupons'); ?>
                </div>
                <?php
                $botao_cupons = get_field("botao_cupons");
                if ($botao_cupons) {
                    $link_url = $botao_cupons['url'];
                    $link_title = $botao_cupons['title'];
                    $link_target = $botao_cupons['target'] ? $botao_cupons['target'] : '_self';
                ?>
                    <a class="btn-primary center-btn" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                        <?php echo $link_title; ?>
                    </a>
                    <?php 
                }
            ?>
            </div>
        </div>
    </div>
</section>
<section id="aquecimentoBF" class="container">
    <div class="row">
        <div class="col-12">
            <h2 class="title-big text-center">
                <?php echo get_field("titulo_aquecimento") ?>
            </h2>
            <p class="text-center">
                <?php echo get_field("descricao_aquecimento") ?>
            </p>
            <?php get_template_part('partials/aquecimento-videos'); ?>
        </div>
    </div>
</section>
<section id="news" class="container">
    <?php get_template_part('partials/news_block'); ?>
</section>
<section id="faq" class="container">
    <div class="row">
        <div class="col-12">
            <h2 class="title">
                <?php echo get_field('titulo_faq'); ?>
            </h2>
            <p>
                <?php echo get_field('subtitulo_faq'); ?>
            </p>
            <?php get_template_part('partials/faq'); ?>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="horarioLives" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <i class="icon-close"></i>
            </button>
            <div class="modal-body">

                <?php if( have_rows('cadastro_horario_das_lives') ): ?>
                <?php while( have_rows('cadastro_horario_das_lives') ): the_row(); 

                $titulo = get_sub_field('titulo');
                $subtitulo = get_sub_field('subtitulo');
                $botao = get_sub_field('botao');
                ?>
                <h2>
                    <?php echo $titulo; ?>
                </h2>
                <p>
                    <?php echo $subtitulo; ?>
                </p>
                <?php
                if ($botao) {
                    $link_url = $botao['url'];
                    $link_title = $botao['title'];
                    $link_target = $botao['target'] ? $botao['target'] : '_self';
                ?>
                    <a class="btn-secondary" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                        <i class="icon-youtube icone"></i>
                        <?php echo  $link_title; ?>
                    </a>
                    <?php
                }
                ?>
                        <?php if( have_rows('horarios') ): ?>
                        <div class="d-lg-block d-md-block d-none">
                            <table>
                                <thead>
                                    <tr>
                                        <th>Live</th>
                                        <th>Data</th>
                                        <th>Horários</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php while( have_rows('horarios') ): the_row(); 
                        $nome_da_live = get_sub_field('nome_da_live');
                        $data_da_live = get_sub_field('data_da_live');
                        $horario_da_live = get_sub_field('horario_da_live');
                    ?>
                                    <tr>
                                        <td>
                                            <strong><?php echo $nome_da_live;?></strong>
                                        </td>
                                        <td>
                                            <?php echo $data_da_live;?>
                                        </td>
                                        <td>
                                            <?php echo $horario_da_live;?>
                                        </td>
                                    </tr>
                                    <?php endwhile; ?>

                                </tbody>
                            </table>
                        </div>

                        <div class="d-lg-none d-md-none d-block">
                            <ul>
                                <?php while( have_rows('horarios') ): the_row(); 
                        $nome_da_live = get_sub_field('nome_da_live');
                        $data_da_live = get_sub_field('data_da_live');
                        $horario_da_live = get_sub_field('horario_da_live');
                    ?>
                                <li>
                                    <strong><?php echo $nome_da_live;?></strong>
                                    <span>
                                <?php echo $data_da_live;?>
                            </span>
                                    <span>
                                <?php echo $horario_da_live;?>
                            </span>
                                </li>
                                <?php endwhile; ?>
                            </ul>
                        </div>
                        <?php endif; ?>
                        <?php endwhile; ?>
                        <?php endif; ?>

            </div>

        </div>
    </div>
</div>

<?php get_footer(); ?>