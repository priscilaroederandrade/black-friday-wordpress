</div>
<!-- /.wrapper -->
<?php get_template_part('partials/youtube_live'); ?>


<footer>
    <div class="sub-footer">
        <div class="container">
            <div class="row d-block d-lg-flex align-items-center justify-content-lg-between justify-content-center">
                <div class="col-auto ">
                    <?php 
                    $linkTecmundo = get_field('link_tecmundo', 'option');
                    if( $linkTecmundo ): 
                        $link_url = $linkTecmundo['url'];
                        $link_title = $linkTecmundo['title'];
                        $link_target = $linkTecmundo['target'] ? $linkTecmundo['target'] : '_self';
                    ?>
                    <a class="button " href="<?php echo esc_url( $link_url ); ?>" target="
                <?php echo esc_attr( $link_target ); ?>">
                        <?php 
                        $logo_tecmundo = get_field('logo_tecmundo', 'option');
                        if( !empty( $logo_tecmundo ) ): ?>
                        <img src="<?php echo esc_url($logo_tecmundo['url']); ?>" alt="<?php echo esc_attr($logo_tecmundo['alt']); ?>" />
                        <?php endif; ?>
                    </a>
                    <?php endif; ?>
                </div>
                <div class="col-auto">
                    <?php
                        if( get_field('exibir_redes_sociais_no_footer', 'option') ) {
                    ?>
                        <div class="redes-sociais-footer">
                            <?php get_template_part('partials/redes_sociais'); ?>
                        </div>
                        <?php
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>

    <div class="nzn_footer">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <?php 
                    $link_nzn = get_field('link_nzn', 'option');

                    if( $link_nzn ): 
                        $link_url = $link_nzn['url'];
                        $link_title = $link_nzn['title'];
                        $link_target = $link_nzn['target'] ? $linkTecmundo['target'] : '_self';
                    
                        $logo_nzn = get_field('logo_nzn', 'option');
                            if( !empty( $logo_nzn ) ): 
                    ?>
                    <a class="logo" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                        <img src="<?php echo esc_url($logo_nzn['url']); ?>" alt="<?php echo esc_attr($logo_nzn['alt']); ?>" />
                    </a>
                    <?php 
                            endif;
                        endif; 
                    ?>
                    <?php if( have_rows('verticais_nzn', 'option') ): ?>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <ul class="verticais_nzn">
                        <?php while( have_rows('verticais_nzn', 'option') ): the_row(); 
                $link_verticais = get_sub_field('link_verticais');
            ?>
                        <li>
                            <?php 
                    if( $link_verticais ): 
                        $link_url = $link_verticais['url'];
                        $link_title = $link_verticais['title'];
                        $link_target = $link_verticais['target'] ? $link_verticais['target'] : '_self';
                        ?>
                            <a class="button" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                                <?php echo esc_html( $link_title ); ?>
                            </a>
                            <?php endif; ?>
                        </li>
                        <?php endwhile; ?>
                    </ul>
                    <?php endif; ?>
                </div>
            </div>
        </div>


    </div>
</footer>
<?php wp_footer(); ?>

</body>

</html>