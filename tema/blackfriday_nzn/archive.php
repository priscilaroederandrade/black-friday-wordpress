<?php
/*
 * Template Categorys
 */
  
 get_header();  ?>

    <div class="header-category">
        <div class="container ">

            <div class="row ">
                <div class="col-lg-8">
                    <h1>
                        <?php single_tag_title(); ?> </h1>
                </div>
            </div>
        </div>
    </div>
    <div class="main-content">
        <div class="container ">

            <div class="row ">
                <div class="col-lg-8">
                    <div class="content-blog">
                        <?php while ( have_posts() ) : the_post(); ?>
                        <div class="blog-post">
                            <?php
                                    if ( has_post_thumbnail() ) {
                                        the_post_thumbnail( 'destaques', array( 'alt' => the_title_attribute( 'echo=0' ), 'class'  => "img-fluid" ) );
                                    }
                                ?>
                                <div class="blog-post-content">
                                    <div class="infos">
                                        <?php 
                                                $categories = get_the_category();
                                                if ( ! empty( $categories ) ) {
                                            ?>
                                        <strong class="infos-<?php echo esc_html( $categories[0]->slug ) ; ?>">
                        <?php 
                                echo '<a href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '" rel="preload">' . esc_html( $categories[0]->name ) . '</a>';
                            } 
                        ?>
                    </strong>
                                    </div>
                                    <?php the_title( sprintf( '<h2 class="title"><a href="%s" rel="preload">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
                                </div>
                        </div>
                        <?php
                                endwhile;
                            ?>
                                                </div>

                            <?php
the_posts_pagination( array(
	'mid_size'  => 2,
	'prev_text' => __( 'Anterior', 'textdomain' ),
	'next_text' => __( 'Próxima', 'textdomain' ),
) );  
wp_reset_postdata(); 
?>
                </div>
                <div class="col-lg-4">
                </div>
            </div>
        </div>
    </div>
    <?php get_footer(); ?>