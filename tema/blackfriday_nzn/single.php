<?php
/*
 * Template Name: Default
 * Template Post Type: post, page, product
 */
  
 get_header();  ?>
        <div class="row ">
            <div class="col-lg-12">
                <?php

			while ( have_posts() ) :
				the_post();

				get_template_part( 'content', 'single' );

			endwhile; 
			?>
            </div>
        </div>

    <?php get_footer(); ?>