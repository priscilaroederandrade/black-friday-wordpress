<div class="container">
    <div class="row">
        <div class="col-lg-9 offset-lg-1">

            <h1 class="blog-post-title">
                <?php the_title(); ?>
            </h1>
            <p class="excerpt">
                <?php echo  get_the_excerpt(); ?>
            </p>

            <div class="infos">
                <?php 
        $categories = get_the_category();
        if ( ! empty( $categories ) ) {
        ?>
                <strong class="infos-<?php echo esc_html( $categories[0]->slug ) ; ?>">
			<?php 
			
					echo '<a href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '" rel="preload">' . esc_html( $categories[0]->name ) . '</a>';
				} 
			?>
		</strong>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-9 offset-lg-1">
            <?php
		the_post_thumbnail( 'full-destaque', array( 'alt' => the_title_attribute( 'echo=0' ), 'class'  => "blog-post-image" ) );
?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-9 offset-lg-1 floatMedia">
            <?php the_content(); ?>
        </div>
    </div>
</div>
<!-- /.blog-post -->