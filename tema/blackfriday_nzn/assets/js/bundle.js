(function($) {
    var carousel = $(".carousel-videos"),
        items = $(".item"),
        currdeg = 0;

    $(".next").on("click", { d: "n" }, rotate);
    $(".prev").on("click", { d: "p" }, rotate);

    if (items.length <= 1) {
        $(".next, .prev").hide();
    } else {
        $(".next, .prev").show();

    }

    function rotate(e) {
        if (e.data.d == "n") {
            currdeg = currdeg - 60;
        }
        if (e.data.d == "p") {
            currdeg = currdeg + 60;
        }
        carousel.css({
            "-webkit-transform": "rotateY(" + currdeg + "deg)",
            "-moz-transform": "rotateY(" + currdeg + "deg)",
            "-o-transform": "rotateY(" + currdeg + "deg)",
            "transform": "rotateY(" + currdeg + "deg)"
        });
        items.css({
            "-webkit-transform": "rotateY(" + (-currdeg) + "deg)",
            "-moz-transform": "rotateY(" + (-currdeg) + "deg)",
            "-o-transform": "rotateY(" + (-currdeg) + "deg)",
            "transform": "rotateY(" + (-currdeg) + "deg)"
        });
    }
    $('.close-newsletter').click(function() {
        $("#newsletter").addClass('hideNews');
    });
    $(window).scroll(function() {

        if ($(window).scrollTop() >= 600) {
            $('#newsletter').css({ 'opacity': '1', 'bottom': '0px', 'display': 'flex' });

        } else {
            $('#newsletter').css({ 'opacity': '0', 'display': 'none' });
        }

    });

    $(window).scroll(function() {
        if ($(window).scrollTop() >= 100) {
            console.log('add');
            $('#fix-btn').css({ 'display': 'block' });
            $('.btn-fix').addClass('fix');
        } else {
            console.log('remove');
            $('#fix-btn').css({ 'display': 'none' });

            $('.btn-fix').removeClass('fix');

        }

    });

    $('#hamburger').click(function() {
        $(this).toggleClass("active");
        $('#menu li').click(function() {
            $('#hamburger').removeClass('active');
            $('.navbar-collapse').removeClass('show');
        });
    });

    $('.wpcf7-form.sent').ajaxComplete(function() {
        $('.news').delay(2000).hide('slow');
        $('.messageok').delay(2000).show('slow');
    });




    $('#top-categories').owlCarousel({
        dots: true,
        loop: true,
        items: 2,
        center: true,
        margin: 10,
    });

    $('#ofertas-selecionadas').owlCarousel({
        dots: true,
        loop: true,
        items: 2,
        center: true,
        margin: 10,
    });

    $('#cupons-area').owlCarousel({
        loop: true,
        margin: 10,
        nav: false,
        items: 2,

    });

    $('#aquecimento-bf').owlCarousel({
        loop: true,
        margin: 10,
        nav: false,
        items: 1,

    });
    $('.close-yotube').click(function() {
        $('.yotube-live').slideToggle("slow");
    });


    $('.arrow').click(function() {
        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top
        }, 500);
        return false;
    });
}(jQuery));