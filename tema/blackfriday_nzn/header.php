<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php wp_head();?>
    <!-- Inicio Schema para FAQ -->
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "FAQPage",
            "mainEntity": [{
                "@type": "Question",
                "name": "O que é a Black Friday?",
                "acceptedAnswer": {
                    "@type": "Answer",
                    "text": "A Black Friday é um dia de ofertas organizado por varejistas, com descontos que vão até 70%. A Black Friday surgiu no Estados Unidos e é comemorada na sexta-feira após o Dia de Ação de Graças."
                }
            }, {
                "@type": "Question",
                "name": "Quando é a Black Friday?",
                "acceptedAnswer": {
                    "@type": "Answer",
                    "text": "Em 2020, a Black Friday será no dia 27 de novembro. A Black Friday é realizada na sexta-feira seguinte ao Dia de Ação de Graças, feriado estadunidense comemorado toda 4ª quinta-feira de novembro."
                }
            }, {
                "@type": "Question",
                "name": "Quando é o Esquenta Black Friday?",
                "acceptedAnswer": {
                    "@type": "Answer",
                    "text": "O esquenta Black Friday é uma série de promoções nas semanas anteriores à Black Friday. Em geral, o aquecimento começa na primeira semana de novembro e vai até a quinta feira antes da Black Friday."
                }
            }, {
                "@type": "Question",
                "name": "Qual a média de descontos da Black Friday?",
                "acceptedAnswer": {
                    "@type": "Answer",
                    "text": "Cada segmento de produtos costuma ter uma média de descontos na Black Friday, com descontos variando entre 10% e 70%. Fique de olho na Black Friday do TecMundo para aproveitar as melhores ofertas!"
                }
            }, {
                "@type": "Question",
                "name": "Como são feitas as seleções de ofertas?",
                "acceptedAnswer": {
                    "@type": "Answer",
                    "text": "A equipe do TecMundo seleciona ofertas da Black Friday de acordo com os produtos mais buscados, maior desconto e a confiabilidade de cada loja."
                }
            }, {
                "@type": "Question",
                "name": "Qual a duração da live de ofertas ao vivo?",
                "acceptedAnswer": {
                    "@type": "Answer",
                    "text": "As lives de ofertas ao vivo do TecMundo duram 1h30, com exceção da Live da virada que dura cerca de 4 horas. As datas e horários de cada live da Black Friday são: 26/11 às 17h - Live de aquecimento; 26/11 às 23h - Live da virada; 27/11 às 11h e 17h - Lives de Black Friday; e 30/11 às 11h e 17h - Lives de Cyber Monday."
                }
            }, {
                "@type": "Question",
                "name": "Como identificar se os descontos da Black Friday são reais?",
                "acceptedAnswer": {
                    "@type": "Answer",
                    "text": "Para saber se as ofertas de Black Friday são reais, pesquise o produto em sites que mostram o histórico de preços. É importante também assegurar que a loja em que você está comprando é confiável."
                }
            }, {
                "@type": "Question",
                "name": "É mais barato comprar no cartão ou no boleto?",
                "acceptedAnswer": {
                    "@type": "Answer",
                    "text": "Isso pode variar de acordo com a loja em que você está comprando. Por isso, é importante prestar atenção às formas e condições de pagamento de cada site."
                }
            }, {
                "@type": "Question",
                "name": "O que é cashback e como funciona?",
                "acceptedAnswer": {
                    "@type": "Answer",
                    "text": "Cashback significa dinheiro de volta. Trata-se de um sistema em que, ao comprar um produto em uma loja online, parte do valor é devolvido para que seja usado em futuras compras ou em dinheiro. Para comprar com cashback na Black Friday, procure por lojas e produtos que ofereçam essa opção."
                }
            }, {
                "@type": "Question",
                "name": "Qual o prazo de entrega médio na Black Friday?",
                "acceptedAnswer": {
                    "@type": "Answer",
                    "text": "O prazo de entrega varia de acordo com a loja em que a compra foi feita. Por isso, é importante ficar de olho no prazo na hora de efetuar o pagamento. Mas, em geral, durante a Black Friday os prazos de entrega são um pouco maiores do que o normal."
                }
            }]
        }
    </script>
    <!-- Fim Schema para FAQ -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-144680-14"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'UA-144680-14');
    </script>
    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-5B9759X');
    </script>
    <!-- End Google Tag Manager -->
</head>

<body <?php body_class(); ?>>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5B9759X"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <header>
        <div class="container">
            <nav class="navbar p-0 navbar-expand-lg">
                <div class="row m-0 w-100 d-flex align-items-center justify-content-lg-between justify-content-center">
                    <div class="col-lg-auto col-12 p-0">
                        <h1 class="logo-name">
                            Black Friday 2020
                        </h1>
                        <?php echo _blackfriday_nzn_get_custom_logo(); ?>
                    </div>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <a id="hamburger" href="#"><span></span></a>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <div class="col-auto m-0 p-0 w-100 row d-lg-flex d-block align-items-center justify-content-end">
                            <?php wp_nav_menu( array( 'theme_general_location' => 'header-menu', 'menu' => 'menu', 'container' => false, 'menu_id' => 'menu', 'menu_class'=>'' ) ); ?>
                            <?php get_template_part('partials/redes_sociais'); ?>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </header>
    <div id="fix-btn">
        <div class="container">

            <?php if( have_rows('hub_de_ofertas', 'option') ): ?>
            <?php while( have_rows('hub_de_ofertas', 'option') ): the_row(); 
                    $link = get_sub_field('link');
                    $link_url = $link['url'];
                    $link_title = $link['title'];
                    $link_target = $link['target'] ? $link['target'] : '_self';
            ?>
            <div class="btn-fix">
                <div>
                    <a class="button" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                        <i class="icon-hub"></i>
                        <?php echo esc_html( $link_title ); ?>
                    </a>
                </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>

        </div>
    </div>

    <div class="wrapper">