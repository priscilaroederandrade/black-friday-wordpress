<?php
/*
Template Name: Search Page
*/
 get_header(); ?>
    <div class="container ">
        <div class="row">
            <div class="col-lg-8">
                <h1><span>Resultados de busca</span></h1>

                <div class="content-blog">
                    <?php
			if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                        <div class="blog-post">
                            <?php
            if ( has_post_thumbnail() ) {
                the_post_thumbnail( 'post-thumbnail', array( 'alt' => the_title_attribute( 'echo=0' ), 'class'  => "img-fluid" ) );
            }
        ?>
                                <div class="blog-post-content">
                                    <div class="infos">
                                        <?php 
                            $categories = get_the_category();
                            if ( ! empty( $categories ) ) {
                        ?>
                                        <strong class="infos-<?php echo esc_html( $categories[0]->slug ) ; ?>">
                            <?php 
                               
                                    echo '<a href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '">' . esc_html( $categories[0]->name ) . '</a>';
                                } 
                            ?>
                        </strong>
                                    </div>
                                    <?php the_title( sprintf( '<h2 class="title"><a href="%s">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
                                </div>
                        </div>
                        <?php
			endwhile; endif;
			?>
                </div>
            </div>
            <!-- /.blog-main -->
            <div class="col-lg-4">
            </div>
        </div>
        <!-- /.row -->
    </div>
    <?php get_footer(); ?>