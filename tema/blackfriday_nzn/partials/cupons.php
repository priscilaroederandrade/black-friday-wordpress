<?php 
$tag_cupons = '';
function getCupons($tag_cupons){
    $request = wp_remote_get( 'https://api-hubofertas.nznweb.com.br/api/v1/oferta/'.$tag_cupons.'' );
    if( is_wp_error( $request ) ) {
        $error_string = $request->get_error_message();
        echo '<div id="message" class="error"><p>' . $error_string . '</p></div>';
    }
   
    $body = wp_remote_retrieve_body( $request );
    
    $data = json_decode( $body );
    

    return $data->dados;
    print_r($result);
}
//https://api-hubofertas.nznweb.com.br/api/v1/oferta/cupons
?>
<?php                  
    $dados = getCupons(get_field("tag_cupons"));
    if( ! empty( $dados ) ) {
?>
<div class=" d-lg-block d-none">
    <div class="cupons-dflex">
        <?php 
        $n = 1;
        $limit = 5;
        foreach( $dados as $post ) {
            if ($n <= $limit) {      
    ?>
        <a href="<?php echo $post->url; ?>" target="_blank" class="card-cupons">
            <img src="<?php echo $post->logo; ?>" alt="" class="img-fluid" />
        </a>
        <?php
        }
        $n++;
        }
    ?>
    </div>
</div>

<div class="d-lg-none d-block">
    <ul id="cupons-area" class="owl-carousel cupons-dflex">
        <?php 
        $n = 1;
        $limit = 5;
        foreach( $dados as $post ) {
            if ($n <= $limit) {      
    ?>
        <li class="d-flex align-items-stretch h-100">
            <a href="<?php echo $post->url; ?>" target="_blank" class="card-cupons">
            <img src="<?php echo $post->logo; ?>" alt="" class="img-fluid" />
        </a>
        </li>
        <?php
        }
        $n++;
        }
    ?>
    </ul>
</div>

<?php                   
    }
?>