<?php if( have_rows('faq_acc') ): ?>

<div class="accordion" id="accordionFaq">
    <?php 
        $f = 0;
        while( have_rows('faq_acc') ): the_row(); 
        $pergunta = get_sub_field('pergunta');
        $resposta = get_sub_field('resposta');
    ?>
    <div class="card">
        <div class="card-header" id="heading<?php echo $f; ?>">
            <h3 class="mb-0">
                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse<?php echo $f; ?>" aria-expanded="<?php if($f == 0) { echo 'true';} else { echo 'false'; } ?>" aria-controls="collapse<?php echo $f; ?>">
                    <?php echo $pergunta; ?>
                </button>
            </h3>
        </div>

        <div id="collapse<?php echo $f; ?>" class="collapse <?php if($f == 0) { echo 'show';} ?>" aria-labelledby="heading<?php echo $f; ?>" data-parent="#accordionFaq">
            <div class="card-body">
                <?php echo $resposta; ?>
            </div>
        </div>
    </div>
    <?php 
        $f++;
        endwhile; 
    ?>
</div>
<?php endif; ?>