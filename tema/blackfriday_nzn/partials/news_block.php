<?php 
$url = 'https://www.tecmundo.com.br';
$urlImg = 'https://img.ibxk.com.br/';
$imgAttr = '?w=704&h=288&mode=crop&scale=both';
$ext = '.jpg';
$utm = '';

function getNews($slugNoticias, $exibir_quantas_noticias){
    $request = wp_remote_get( 'https://www.tecmundo.com.br/api/v1/tag?tags='.$slugNoticias.'&top='.$exibir_quantas_noticias.'' );
    
                    if( is_wp_error( $request ) ) {
                        $error_string = $request->get_error_message();
                        echo '<div id="message" class="error"><p>' . $error_string . '</p></div>';
                    }
    
                    $body = wp_remote_retrieve_body( $request );
    
                    $data = json_decode( $body );
                    
                
                    $result = $data[0];
    return $result->dados;
}

?>
<div class="row">
    <?php 
   $p = 0;
   $limit = get_field("carrousel_news_amount");                                            
                                      
                                       
   $dados = getNews(get_field("slugNoticias"), get_field("exibir_quantas_noticias"));
   foreach($dados as $news){
    if ($p <= $limit) { 
    foreach( $dados as $post ) {
           if ($p == 0) {
?>
    <div class="col-lg-7 pb-3">
        <a target="_blank" class="card-news" href="<?php echo $url . $post->url . $utm; ?>">
            <img class="img-fluid img-1" src="<?php echo $urlImg . $post->imagem . $ext; ?>" alt="<?php echo $post->titulo; ?>" />
            <h2 class="title-news1"><?php echo $post->titulo; ?></h2>
        </a>
    </div>
    <?php
           } elseif($p >= 1 && $p <= 3) {
            if($p == 1){
    ?>
        <div class="col-lg-5 pb-3">
            <?php
            }?>
                <div class="row">
                    <a class="d-flex pb-3 card-news" target="_blank" href="<?php echo $url . $post->url . $utm; ?>">
                        <div class="col-5">
                            <img class="img-fluid img-2" src="<?php echo $urlImg . $post->imagem . $ext; ?>" alt="<?php echo $post->titulo; ?>" />
                        </div>
                        <div class="col-7 pl-0">
                            <h2 class="title-news2">
                                <?php echo $post->titulo; ?>
                            </h2>

                        </div>
                    </a>
                </div>

                <?php
            if($p == 3){
        ?>
        </div>
</div>

<div class="row ">
    <div class="col-lg-8 col-12">

        <?php

            }

            ?>


            <?php
           } else {
        ?>

                <a class=" d-md-flex d-lg-flex d-none pb-5 card-news" target="_blank" href="<?php echo $url . $post->url . $utm; ?>">
                    <div class="col-3 p-0">
                        <img class="img-fluid img-3" src="<?php echo $urlImg . $post->imagem . $ext; ?>" alt="<?php echo $post->titulo; ?>" />
                    </div>
                    <div class="col-9 pl-3">
                        <h2 class="title-news3">
                            <?php echo $post->titulo; ?>
                        </h2>
                        <small>
                            <?php echo $post->publicadoHa; ?>
                        </small>
                    </div>
                </a>

                <?php
                }
           $p++;
                }
            }
        }
                
                $botao_todas_as_noticias = get_field("botao_todas_as_noticias");
                if ($botao_todas_as_noticias) {
                    $link_url = $botao_todas_as_noticias['url'];
                    $link_title = $botao_todas_as_noticias['title'];
                    $link_target = $botao_todas_as_noticias['target'] ? $botao_todas_as_noticias['target'] : '_self';
                ?>
                    <a class="btn-primary center-btn text-center" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                        <?php echo $link_title; ?>
                    </a>
                    <?php 
                }
                ?>
    </div>
    <div class="col-lg-4 col-12 pl-0 pr-0 pt-3">
        <div class="mega-widget">
            <?php
        $botao_mega = get_field("botao_mega");
        $logo_mega = get_field("logo_mega");
        if ($botao_mega) {
            $link_url = $botao_mega['url'];
            $link_title = $botao_mega['title'];
            $link_target = $botao_mega['target'] ? $botao_mega['target'] : '_self';
        ?>
                <a class="btn-mega text-center center-btn" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                <img class="img-fluid" src="<?php echo esc_url($logo_mega['url']); ?>" alt="<?php echo esc_attr($logo_mega['title']); ?>" />
            </a>

                <?php 
        }
        ?>
                <?php
                $widget_mega = get_field("widget_mega");

                echo $widget_mega;
            ?>
        </div>
    </div>
</div>