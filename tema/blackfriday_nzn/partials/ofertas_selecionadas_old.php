<h3>
    <?php the_field('titulo_superOferta'); ?>
    <?php
$link = get_field('link_para_o_hub');
if( $link ): 
    $link_url = $link['url'];
    $link_title = $link['title'];
    $link_target = $link['target'] ? $link['target'] : '_self';
    ?>
        <a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
            <?php echo esc_html( $link_title ); ?>
        </a>
        <?php endif; ?>
</h3>
<?php 

function getOfertas($campanhaSlug,$tag){
    $request = wp_remote_get( 'https://api-hubofertas.nznweb.com.br/api/v1/oferta/'.$campanhaSlug.'/tag/'.$tag );
    
                    if( is_wp_error( $request ) ) {
                        $error_string = $request->get_error_message();
                        echo '<div id="message" class="error"><p>' . $error_string . '</p></div>';
                    }
                    $body = wp_remote_retrieve_body( $request );;
                    $data = json_decode( $body );
                    $result = $data;
                    return $result->dados;
}
//https://api-hubofertas.nznweb.com.br/api/v1/oferta/blackfriday-2019/tag/super-oferta
?>


<?php 
    $dados = getOfertas(get_field("campanhaSlug"), get_field("tag"));
    if( ! empty( $dados ) ) {
?>
<div class="produtos-dflex">
    <?php 
        $n = 1;
        $p = 0;
        $limit = 3;                          
        foreach( $dados->paginas[$p] as $pagina ) {
            if(sizeof($dados->paginas) < ($p + 1)){
                $p++;
            }
            foreach( $pagina as $post) {
            if ($n <= $limit) { 
                  
?>
    <a href="<?php echo $post->linkEncurtado; ?>" target="_blank" class="card">

        <h4><i class="icon-star"></i>
            <?php echo $post->tag; ?>
        </h4>
        <img src="<?php echo $post->imagem; ?>" alt="" class="img-fluid" />
        <h5>
            <?php echo $post->produto; ?>
        </h5>
        <div class="precoAnterior">
            <span>R$ <?php echo $post->precoAnteriorFormatado; ?></span>
        </div>
        <div class="preco">
            <span><small>R$ </small><?php echo $post->precoFormatado; ?></span>
        </div>
    </a>
    <?php
        }
        $n++;
            }
        }
        


?>
</div>
<?php                   
        
    }
?>