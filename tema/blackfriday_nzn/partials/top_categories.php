<?php 
$campanhaSlug = '';
$limite_de_categorias = '';
function getCategories($campanhaSlug, $limite_de_categorias){
    $request = wp_remote_get( 'https://api-hubofertas.nznweb.com.br/api/v1/categoria?campanhaSlug='.$campanhaSlug.'&limite_de_categorias='.$limite_de_categorias.'' );
    
                    if( is_wp_error( $request ) ) {
                        $error_string = $request->get_error_message();
                        echo '<div id="message" class="error"><p>' . $error_string . '</p></div>';
                    }
    
                    $body = wp_remote_retrieve_body( $request );
    
                    $data = json_decode( $body );
                    
                
                    return $data->dados;

}

//https://api-hubofertas.nznweb.com.br/api/v1/categoria?campanhaSlug=blackfriday-2019
?>


<?php 
                    
                    $dados = getCategories(get_field("campanhaSlug"), get_field("limite_de_categorias"));
                    if( ! empty( $dados ) ) {

                   ?>
<div class=" d-lg-block d-none">
    <div class="categories-dflex">
        <?php 
                                $n = 1;
                                $limit = get_field("limite_de_categorias");
                                        foreach( $dados as $post ) {
                                                                if($post->id  == 1 || $post->id  == 2 || $post->id  == 9 || $post->id  == 6 || $post->id  == 7 || $post->id  == 8) {

                                        if ($n <= $limit) {      
                            ?>

        <a href="https://www.tecmundo.com.br/blackfriday/hub/ofertas/<?php echo $post->slug; ?>" class="card">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/<?php echo $post->slug; ?>.png" alt=""
            class="img-fluid" />
        <span><?php echo $post->nome; ?></span>


    </a>

        <?php
                                        }
                                        $n++;
                                        }
                    }
                }
                                    
                            ?>
    </div>
</div>
<!-- mobile -->

<div class="d-lg-none d-block">
    <ul id="top-categories" class="owl-carousel categories-dflex">
        <?php 
        if( ! empty( $dados ) ) {
        $n = 1;
        $limit = get_field("limite_de_categorias");
                foreach( $dados as $post ) {
                                        if($post->id  == 1 || $post->id  == 2 || $post->id  == 9 || $post->id  == 6 || $post->id  == 7 || $post->id  == 8) {

                if ($n <= $limit) {   
?>
        <li class="d-flex align-items-stretch h-100">
            <a href="https://www.tecmundo.com.br/blackfriday/hub/ofertas/<?php echo $post->slug; ?>" class="card">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/<?php echo $post->slug; ?>.png" alt=""
                class="img-fluid" />
            <span><?php echo $post->nome; ?></span>
    
    
        </a>
        </li>
        <?php
    }
    $n++;
    }
}



?>

    </ul>
</div>
<?php                   
                        }
                    ?>