<h3>
    <?php the_field('titulo_superOferta'); ?>
    <?php
    $link = get_field('link_para_o_hub');
    if( $link ): 
        $link_url = $link['url'];
        $link_title = $link['title'];
        $link_target = $link['target'] ? $link['target'] : '_self';
    ?>
    <a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
        <?php echo esc_html( $link_title ); ?>
    </a>
    <?php endif; ?>
</h3>
<?php 
    $request = wp_remote_get( 'https://api-hubofertas.nznweb.com.br/api/v1/oferta/blackfriday-2019/tag/super-oferta');
    
    if( is_wp_error( $request ) ) {
        $error_string = $request->get_error_message();
        echo '<div id="message" class="error"><p>' . $error_string . '</p></div>';
    }
    $body = wp_remote_retrieve_body( $request );;
    $data =  json_decode($body) ;
    $result = $data;
    $dados= $result->dados;
    $paginas= $dados->paginas;
    $itens= $paginas[0]->itens;
    //print_r($itens);
    if( ! empty( $itens ) ) {

    ?>
<div class=" d-lg-block d-none">

    <div class="produtos-dflex">
        <?php
    $n = 1;
    $limit = 3;
    foreach ($itens as $item => $post) {
        if ($n <= $limit) { 
?>
        <a href="<?php echo $post->linkEncurtado; ?>" target="_blank" class="card">
            <h4><i class="icon-star"></i>
                <?php echo $post->tag; ?>
            </h4>
            <img src="<?php echo $post->imagem; ?>" alt="" class="img-fluid" />
            <h5>
                <?php echo $post->produto; ?>
            </h5>
            <div class="precos">
                <div class="precoAnterior">
                    <span>R$ <?php echo $post->precoAnteriorFormatado; ?></span>
                </div>
                <div class="preco">
                    <span><small>R$ </small><?php echo $post->precoFormatado; ?></span>
                </div>
            </div>
        </a>
        <?php
        }
        $n++;
    }
?>
    </div>
</div>


<div class="d-lg-none d-block">
    <ul id="ofertas-selecionadas" class="owl-carousel produtos-dflex">
        <?php
        $n = 1;
        $limit = 3;
        foreach ($itens as $item => $post) {
            if ($n <= $limit) { 
    ?>
        <li class="d-flex align-items-stretch h-100">
        <a href="<?php echo $post->linkEncurtado; ?>" target="_blank" class="card">
            <h4><i class="icon-star"></i>
                <?php echo $post->tag; ?>
            </h4>
            <img src="<?php echo $post->imagem; ?>" alt="" class="img-fluid" />
            <h5>
                <?php echo $post->produto; ?>
            </h5>
            <div class="precos">
                <div class="precoAnterior">
                    <span>R$ <?php echo $post->precoAnteriorFormatado; ?></span>
                </div>
                <div class="preco">
                    <span><small>R$ </small><?php echo $post->precoFormatado; ?></span>
                </div>
            </div>
        </a>
        </li>
        <?php
    }
    $n++;
}
?>
    </ul>
</div>

<?php } ?>