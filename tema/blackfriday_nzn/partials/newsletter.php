<div id="newsletter">
    <div class="container">
        <div class="row">
            <div class="col-12 p-0 news">
                <h3 class="title-simple ">
                    <?php the_field('titulo_newsletter'); ?>
                </h3>

<!-- Begin Mailchimp Signup Form -->

<link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">

<style type="text/css">
    #mc_embed_signup {
        background: #fff;
        clear: left;
        font: 14px Helvetica, Arial, sans-serif;
    }
    /* Add your own Mailchimp form style overrides in your site stylesheet or in this style block.

We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
</style>

<div id="mc_embed_signup">

    <form action="https://nzn.us16.list-manage.com/subscribe/post?u=afac42edb8d527d4d01a664a0&id=2673a45d6e" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>

        <div id="mc_embed_signup_scroll">
            <div class="mc-field-group">

                <label for="mce-FNAME">Nome <span class="asterisk">*</span>

</label>

                <input type="text" value="" placeholder="Nome" name="FNAME" class="required" id="mce-FNAME">

            </div>

            <div class="mc-field-group">

                <label for="mce-EMAIL">Email <span class="asterisk">*</span>

</label>

                <input type="email" placeholder="E-mail" value="" name="EMAIL" class="required email" id="mce-EMAIL">

            </div>

            <div id="mce-responses" class="clear">

                <div class="response" id="mce-error-response" style="display:none"></div>

                <div class="response" id="mce-success-response" style="display:none"></div>

            </div>
            <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->

            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_afac42edb8d527d4d01a664a0_2673a45d6e" tabindex="-1" value=""></div>

            <div class="clear"><input type="submit" value="Cadastrar" name="subscribe" id="mc-embedded-subscribe" class="button"></div>

        </div>

    </form>

</div>

<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script>
<script type='text/javascript'>
    (function($) {
        window.fnames = new Array();
        window.ftypes = new Array();
        fnames[1] = 'FNAME';
        ftypes[1] = 'text';
        fnames[0] = 'EMAIL';
        ftypes[0] = 'email';
    }(jQuery));
    var $mcj = jQuery.noConflict(true);
</script>

<!--End mc_embed_signup-->
                <?php // echo do_shortcode(get_field('shortcode_newsletter')); ?>
            </div>
            <div class="messageok">
                <p class="wpcf7-response-output">realizado com sucesso! </p>
            </div>
        </div>
    </div>
    <button class="close-newsletter" aria-label="fechar newsletter">
        <i class="icon-close"></i>
    </button>
</div>

