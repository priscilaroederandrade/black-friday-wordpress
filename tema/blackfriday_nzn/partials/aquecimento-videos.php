<?php if( have_rows('videos') ):?>
<div class=" d-lg-block d-none">

    <div class="base-carousel">
        <div class="carousel-videos">
            <?php
                        $n = 0;
                        while( have_rows('videos') ): the_row(); 
                        $url_video = get_sub_field('url_video');
                    ?>
            <div class="itemCard<?php echo $n; ?> itemCard">
                <div class="item">
                    <?php echo $url_video; ?>
                </div>
            </div>
            <?php 
                        $n++;
                        endwhile; 
                        
                    ?>
        </div>
        <div class="controls">
            <div class="next"><i class="icon-arrow"></i></div>
            <div class="prev"><i class="icon-arrow"></i></div>
        </div>
    </div>
</div>
<div class="d-lg-none d-block">
    <ul id="aquecimento-bf" class="owl-carousel">
        <?php
            $n = 0;
            while( have_rows('videos') ): the_row(); 
            $url_video = get_sub_field('url_video');
        ?>
        <li>
            <div class="itemCard<?php echo $n; ?> itemCard">
                <div class="item">
                    <?php echo $url_video; ?>
                </div>
            </div>
        </li>
        <?php 
            $n++;
            endwhile; 
        ?>
    </ul>
</div>
<?php endif; ?>