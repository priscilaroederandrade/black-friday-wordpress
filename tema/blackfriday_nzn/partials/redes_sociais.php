<h2 class="title-simple d-lg-none d-block">
    Siga-nos!
</h2>
<ul id="redes-sociais">
    <?php if( have_rows('youtube', 'option') ): ?>
    <?php while( have_rows('youtube', 'option') ): the_row(); 
                $icone = get_sub_field('icone_class');
                $link = get_sub_field('link');
                $link_url = $link['url'];
                $link_title = $link['title'];
                $link_target = $link['target'] ? $link['target'] : '_self';
        ?>

    <li>
        <a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
            <i class="<?php echo $icone; ?>"></i>
            </a>
    </li>
    <?php endwhile; ?>
    <?php endif; ?>
    <?php if( have_rows('instagram', 'option') ): ?>
    <?php while( have_rows('instagram', 'option') ): the_row(); 
                $icone = get_sub_field('icone_class');
                $link = get_sub_field('link');
                $link_url = $link['url'];
                $link_title = $link['title'];
                $link_target = $link['target'] ? $link['target'] : '_self';
        ?>

    <li>
        <a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                <i class="<?php echo $icone; ?>"></i>
            </a>
    </li>
    <?php endwhile; ?>
    <?php endif; ?>
    <?php if( have_rows('twitter', 'option') ): ?>
    <?php while( have_rows('twitter', 'option') ): the_row(); 
                $icone = get_sub_field('icone_class');
                $link = get_sub_field('link');
                $link_url = $link['url'];
                $link_title = $link['title'];
                $link_target = $link['target'] ? $link['target'] : '_self';
        ?>

    <li>
        <a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                <i class="<?php echo $icone; ?>"></i>
            </a>
    </li>
    <?php endwhile; ?>
    <?php endif; ?>
    <?php if( have_rows('facebook', 'option') ): ?>
    <?php while( have_rows('facebook', 'option') ): the_row(); 
                $icone = get_sub_field('icone_class');
                $link = get_sub_field('link');
                $link_url = $link['url'];
                $link_title = $link['title'];
                $link_target = $link['target'] ? $link['target'] : '_self';
        ?>

    <li>
        <a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                <i class="<?php echo $icone; ?>"></i>
            </a>
    </li>
    <?php endwhile; ?>
    <?php endif; ?>
    <?php if( have_rows('hub_de_ofertas', 'option') ): ?>
    <?php while( have_rows('hub_de_ofertas', 'option') ): the_row(); 
                $link = get_sub_field('link');
                $link_url = $link['url'];
                $link_title = $link['title'];
                $link_target = $link['target'] ? $link['target'] : '_self';
        ?>
    <li>
        <a class="button" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
            <i class="icon-hub"></i>
            <?php echo esc_html( $link_title ); ?>
        </a>
    </li>
    <?php endwhile; ?>
    <?php endif; ?>

</ul>