<?php
    if( get_field('esta_tendo_live') ) {
?>
    <div class="yotube-live">
        <button class="close-yotube" aria-label="fechar youtube live">
        <i class="icon-close"></i>
    </button>
        <h3>
            <div class="pulse-box">
                <div class="pulse-css"></div>
            </div> Participe da nossa live de ofertas!</h3>
        <?php  echo do_shortcode('[embedyt] https://www.youtube.com/embed/live_stream?channel=UCdmGjywrxeOPfC7vDllmSgQ[/embedyt]'); ?>
        <!-- tecmundo UCdmGjywrxeOPfC7vDllmSgQ -->
        <!-- voxel  UCupqHNRTEHjNtq9X8djWyaQ -->
        <!-- mega curioso UCsg11Ullu0dBrNXTrDBKOXA -->
    </div>
    <?php
    }
?>