<?php if( have_rows('clientes') ): ?>
<div class="motion-row">
    <div class="animation">
        <?php
            $array = array();
          
                foreach( $array as $value ) {
                    
                    while( have_rows('clientes') ) : the_row();
                    $logo_imagem = get_sub_field('logo_imagem');
                    $link = get_sub_field('link_');

                        if( $link ): 
                        $link_url = $link['url'];
                        $link_title = $link['title'];
                        $link_target = $link['target'] ? $link['target'] : '_self';
                        ?>
            <a class="itens" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                            <img src="<?php echo esc_url($logo_imagem['url']); ?>" alt="<?php echo esc_attr($logo_imagem['alt']); ?>" />
                        </a>
            <? 
                    endif; 
                    endwhile; 
                } ?>
    </div>
    <?php endif; ?>